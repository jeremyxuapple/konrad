import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../../services/connection.service';

@Component({
  selector: 'game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  constructor(
    private connectionService: ConnectionService
  ) { }

  getGameList() {
    let url = 'http://gd2.mlb.com/components/game/mlb/year_2014/month_03/day_29/master_scoreboard.json';
    this.connectionService.get(url).subscribe((res) => {
        console.log(res);
    });

  }

  ngOnInit() {
    this.getGameList();
  }

}
