import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';


import { AppComponent } from './app.component';
import { GameListComponent } from './components/game-list/game-list.component';
import { GameDetailComponent } from './components/game-detail/game-detail.component';

import { ConnectionService } from './services/connection.service';

@NgModule({
  declarations: [
    AppComponent,
    GameListComponent,
    GameDetailComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [ConnectionService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
