import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Headers, RequestOptions, URLSearchParams, Http, RequestMethod, Request } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ConnectionService {

  constructor(
    private client: HttpClient,
    // private http: Http,
  ) {

 }

  get(url): Observable<any> {
    return this.client.get(url);
  }

}
